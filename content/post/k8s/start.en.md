---
title: Studying for CKA - Start
description: Some steps until CKA
date: 2021-08-28T12:00:00-03:00
draft: false
categories:
  - Software Engineer
  - CKA
  - sysadmin
tags:
  - certification
  - kubernetes
  - linux
---

# Starting


I bought a [CKA certification](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) voucher in 2020 and postpone my study for several and different reasons. Now I decide to begin, and the voucher **expires** in November.

So, I decided to make some notes about the process, so I'm sorry if this page won't appear didactic.

During my study I mixed sources, like:
  - [LinuxTips K8s' Course - lang ptbr](https://www.linuxtips.io/products/descomplicando-o-kubernetes)
  - [TechWorld with Nana - a lot of videos - lang en](https://www.youtube.com/watch?v=X48VuDVv0do&list=RDCMUCdngmbVKX1Tgre699-XLlUA&start_radio=1)
  - [Kubernetes Official Docs - lang en/ptbr](https://kubernetes.io/docs/)

First of all, need to check the previous knowledge necessary, and there are some points, showed below.

 * if you found an error or want to suggest something, feel free to comment

## About CKA & The exam
 > The Certified Kubernetes Administrator (CKA) program was created by the Cloud Native Computing Foundation (CNCF), in collaboration with The Linux Foundation, to help develop the Kubernetes ecosystem.
 > -- [CKA](https://www.cncf.io/certification/cka/)

 > The Certified Kubernetes Administrator (CKA) certification  is designed to ensure that certification holders have the skills, knowledge, and competency to perform the responsibilities of Kubernetes Administrators. The CKA certification allows certified administrators to quickly establish their credibility and value in the job market, and also allowing companies to more quickly hire high-quality teams to support their growth.
 > -- [Linux Foundation - FAQ](https://docs.linuxfoundation.org/tc-docs/certification/faq-cka-ckad-cks)

Questions are grouped as below (source: [Linux Foundation - CKA](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) ):

| Domains & Competencies |
| ------- | ---- |
| Storage | 10% |
| Troubleshooting | 30% |
| Workloads and Scheduling | 15% |
| Cluster Architecture,installation & configuration   |  25% |
| Services & Networking | 20%  |

---

Storage
: Understand storage classes, persistent volumes
: Understand volume mode, access modes and reclaim policies for volumes
: Understand persistent volume claims primitive
: Know how to configure applications with persistent storage 

Troubleshooting
: Evaluate cluster and node logging
: Understand how to monitor applications
: Manage container stdout & stderr logs
: Troubleshoot application failure
: Troubleshoot cluster component failure
: Troubleshoot networking

Workloads & Scheduling
: Understand deployments and how to perform rolling update and rollbacks
: Use ConfigMaps and Secrets to configure applications
: Know how to scale applications
: Understand the primitives used to create robust, self-healing, application deployments
: Understand how resource limits can affect Pod scheduling
: Awareness of manifest management and common templating tools

Cluster Architecture, Installation & Configuration
: Manage role based access control (RBAC)
: Use Kubeadm to install a basic cluster
: Manage a highly-available Kubernetes cluster
: Provision underlying infrastructure to deploy a Kubernetes cluster
: Perform a version upgrade on a Kubernetes cluster using Kubeadm
: Implement etcd backup and restore

Services & Networking
: Understand host networking configuration on the cluster nodes
: Understand connectivity between Pods
: Understand ClusterIP, NodePort, LoadBalancer service types and endpoints
: Know how to use Ingress controllers and Ingress resources
: Know how to configure and use CoreDNS
: Choose an appropriate container network interface plugin

---


    The exam:
    - Languages: English, Japanese and Simplified Chinese
    - Online
    - Duration: 2 hours
    - Valid for 3 years
    - Free Retake
           * as long as the voucher has been acquired at Linux Foundation, and
             not marked as SINGLE-ATTEMPT
    - PDF Certificate and Digital Badge
    - Kubernetes v1.21
    - Exam simulator - *check conditions

Sources:

  - [CKA](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/)

  - [Exam Retake Policy](https://training.linuxfoundation.org/about/policies/certification-exam-retake-policy/)

  - [About Exam Simulator](https://www.cncf.io/announcements/2021/06/02/linux-foundation-kubernetes-certifications-now-include-exam-simulator/)

  - [FAQ - CKA / CKAD & CKS](https://docs.linuxfoundation.org/tc-docs/certification/faq-cka-ckad-cks)

### What is allow?

 According to the documentation, during the exam, candidates may:
 - review the Exam content instructions that are presented in the command line terminal
 - review Documents installed by the distribution (i.e. /usr/share and its subdirectories)
 - use their Chrome or Chromium browser to open one additional tab in order to access assets at: https://kubernetes.io/docs/, https://github.com/kubernetes/,  https://kubernetes.io/blog/ and their subdomains. This includes all available language translations of these pages (e.g. https://kubernetes.io/zh/docs/)

## Knowledge

  Some baggage necessary,

 - Linux / Most of Command Line:
   - [Introduction to Linux - Free Course](https://training.linuxfoundation.org/training/introduction-to-linux/)
   - curl / wget
   - apt / tar
   - modprobe
 - Bash
   - [Bash Guide for Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html)
   - [Linux Shell Programming - lang ptbr](https://temporealeventos.com.br/programacao-shell-linux-com-o-mestre-julio-cezar-neves/)
 - Firewall / iptables:
   - [What Is iptables and How to Use It?](https://medium.com/skilluped/what-is-iptables-and-how-to-use-it-781818422e52)
   - [The Beginner’s Guide to iptables, the Linux Firewall](https://www.howtogeek.com/177621/the-beginners-guide-to-iptables-the-linux-firewall/)
 - Docker / Containers
   - [Book - Uncomplicating Docker - lang ptbr](https://livro.descomplicandodocker.com.br/chapters/chapter_00.html)
   - [Book - Docker for Developers - lang ptbr](https://leanpub.com/dockerparadesenvolvedores)
   - [Docker - From Zero to Hero](youtube.com/watch?v=3c-iBn73dDE)
   - [Containers vs VMs: What is difference?](https://www.youtube.com/watch?v=cjXI-yxqGTI)

## What is Kubernetes ???

According to Kubernetes Docs:

> Kubernetes is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation. It has a large, rapidly growing ecosystem. Kubernetes services, support, and tools are widely available
> -- [Kubernetes Docs](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)

More about:
   - [What is Kubernetes | Kubernetes explained in 15 mins](https://www.youtube.com/watch?v=VnvRFRk_51k)
   - [Kubernetes // Programmer's Dictionary - lang ptbr](https://www.youtube.com/watch?v=mVL0nOM3AGo)
   - [What is Kubernetes](https://www.redhat.com/en/topics/containers/what-is-kubernetes)

Kubernetes, after deployed, provides a cluster. This cluster is composed by Nodes, with different and well defined tasks.

## Pod - An essencial part

> **Pods**: are the smallest deployable units of computing that you can create and manage in Kubernetes.
> -- [Kubernetes Docs - Pods](https://kubernetes.io/docs/concepts/workloads/pods/)

A Pod shares the context of: namespaces, cgroups and other aspects of isolation.

It's common to be confused about Containers and Pods. On a simple way, a container run a microservices and a Pod must contain 1 or more containers.

A Pod act as a Host that provides several and diferents containers.

In general, when a Pod has multiple containers it's because they need to work together, share resources like network, mount points (directories or storages linked).

Kubernetes manages the Pods and if it's necessary to run more instances, Kubernetes increase the number of Pods, scaling the application horizontally, called as Replication. And, plus, Kubernetes provides auto-healing and scaling checking the workload resources (cpu, memory, etc...).

Below an example of a Pod, with a single container

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  # Name of pod
  name: hello
spec:
  template:
    # This is the pod template
    spec:
      containers:
      - name: hello
        # Container
        image: busybox
        command: ['sh', '-c', 'echo "Hello, Kubernetes!" && sleep 3600']
      restartPolicy: OnFailure
    # The pod template ends here
```

Below an example of a Pod, with a multiple container, using same directory

   * this sample uses a directory on node, ephemeral storage

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: two-containers
spec:
  restartPolicy: Never
  volumes:
  - name: shared-data
    emptyDir: {}
  containers:
  - name: nginx-container
    image: nginx
    volumeMounts:
    - name: shared-data
      mountPath: /usr/share/nginx/html
  - name: debian-container
    image: debian
    volumeMounts:
    - name: shared-data
      mountPath: /pod-data
    command: ["/bin/sh"]
    args: ["-c", "echo Hello from the debian container > /pod-data/index.html"]
```

References for this topic:
  1. [Pods and Containers](https://www.youtube.com/watch?v=5cNrTU6o3Fw)
  2. [What's the difference between a pod, a cluster, and a container?](https://enterprisersproject.com/article/2020/9/pod-cluster-container-what-is-difference#:~:text=%E2%80%9CA%20container%20runs%20logically%20in,tight%20logical%20borders%20called%20namespaces.%E2%80%9D)

## Kubernetes components

  > Kubernetes has a tons of components, but most of the time you are going to be working with just a handful of them
  > -- Janashia, Nana (TechWorld)

It's true, but won't be that easy, the **certification will cover** all of them, so it's too important to understand and learn each one of them.

The previously cited Nodes has different categories:
  - Worker - host the Pods (a set of running containers)
    - Every cluster has at least one worker node
  - Control Plane - manages the worker nodes and the Pods
    - The Pod management could define which Worker Node will be use to run/instance the Pod
