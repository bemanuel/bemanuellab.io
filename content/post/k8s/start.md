---
title: Estudando para a certificação CKA - O Início
description: Alguns passos até a certificação
date: 2021-08-28T12:00:00-03:00
draft: false
categories:
  - Software Engineer
  - CKA
  - sysadmin
tags:
  - certificação
  - kubernetes
  - linux
---

# O Início

Comprei o voucher para a [certificação CKA](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) em 2020 e posterguei meu estudo por diversas razões. Agora tomei a decisão de começar a estudar, além do fato que o voucher **expira** em Novembro.

Então, decidi fazer algumas anotações sobre o processo de estudo, por isso me desculpe se a página não parecer muito didática.

Nos meus estudos eu utilizo uma mescla de fontes, como:
  - [LinuxTips K8s' Course - ling ptbr](https://www.linuxtips.io/products/descomplicando-o-kubernetes)
  - [TechWorld with Nana - diversos vídeos - ling en](https://www.youtube.com/watch?v=X48VuDVv0do&list=RDCMUCdngmbVKX1Tgre699-XLlUA&start_radio=1)
  - [Kubernetes - Documentação Oficial - ling en/ptbr](https://kubernetes.io/pt-br/docs/)

Antes de qualquer coisa verifiquei o que eu precisava ter como conhecimento prévio, identifique alguns pontos, os quais cito abaixo.

 * por favor, se encontrar algum erro ou deseja sugerir algo, sinta-se a vontade para comentar

 ## Sobre a CKA + o exame
  > O programa __Certified Kubernetes Administrator (CKA)__ foi desenvolvido pela Cloud Native Computing Foundation (CNCF), em parceria com a The Linux Foundation, com o objetivo de desenolver o ecosistema do Kubernetes.
  > -- [CKA](https://www.cncf.io/certification/cka/)

  > A certificação, Certified Kubernetes Administrator (CKA), é preparada de forma a garantir que seus certificados tenham a habilidade, conhecimento e competência de assumir responsabilidades como Administrador Kubernetes. A certificação CKA garante a esses administradores uma forma de garantir e trazer credibilidade para o mesmo em relação ao mercado de trabalho, ainda permite que empresas contratem/montem de maneira mais rápida uma equipe qualificada de modo a atender suas demandas.
  > -- [Linux Foundation - FAQ](https://docs.linuxfoundation.org/tc-docs/certification/faq-cka-ckad-cks)

 Abaixo uma descrição do escopo da prova CKA (source: [Linux Foundation - CKA](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) ):

 | Domínios & Competências |
 | ------- | ---- |
 | Armazenamento/Storage | 10% |
 | Resolução de problemas/Troubleshooting | 30% |
 | Workloads e Scheduling | 15% |
 | Arquitetura do Cluster, instalação e configuração   |  25% |
 | Serviços & Rede/Conectividade | 20%  |

 ---

 Armazenamento/Storage
 : Entender o que são: storage classes, persistent volumes
 : Entender o que são: volume mode, access modes e reclaim policies em volumes
 : Entender o funcionamento de pvc's
 : Saber configurar aplicações com persistent storage

 Resolução de problemas/Troubleshooting
 : Avaliar/Analisar um cluster e logs dos nodes
 : Saber como monitorar aplicações
 : Gerir a saída de logs dos containers: stdout & stderr
 : Solucionar falhas de aplicação
 : Solucionar falhas de componentes no cluster
 : soulucionar falhas de conectividade/rede

 Workloads e Scheduling
 : Entender o funcionamento de deployments e como executar rolling update e rollbacks
 : Usar ConfigMaps e Secrets para configurar aplicações
 : Saber como escalar aplicações
 : Saber os principios usados para criar/publicar aplicações, estabilidade, self-healing
 : Saber como os limites de recursos podem afetar o Scheduling de Pods
 : Conhecer ferramentas para gestão de manifestos e templates

 Arquitetura do Cluster, instalação e configuração
 : Gerir role based access control (RBAC)
 : Usar Kubeadm para instalar um cluster
 : Gerir um Cluster Kubernetes com HA
 : Provisionar a infra necessária para levantar um cluster Kubernetes
 : Executar a atualização do cluster usando o Kubeadm
 : Implementar o backup e o restore do etcd

 Serviços & Rede/Conectividade
 : Compreender o funcionamento e configuração das redes entre os nodes/nós do cluster
 : Compreender como funciona a conectividade entre os Pods
 : Compreender os tipos de serviço: ClusterIP, NodePort, LoadBalancer; e endpoints
 : Saber usar controladores e recursos Ingress
 : Configurar e usar o CoreDNS
 : Saber escolher o plugin que proverá a container network interface (cni) mais indicada

 ---


     O exame:
     - Disponível nas Línguas: Inglês, Japônes e Chinês simplificado Chinese
     - Online
     - Duração: 2 horas
     - Válido por 3 anos
     - Retake Gratuito
            * desde que, no momento de aplicação do voucher, não exteja identificado como SINGLE-ATTEMPT
     - Certificad em PDF e Badge Digital
     - Kubernetes v1.21
     - Simulador do Exame - verifique antes

 Fontes:

   - [CKA](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/)

   - [Exam Retake Policy](https://training.linuxfoundation.org/about/policies/certification-exam-retake-policy/)

   - [About Exam Simulator](https://www.cncf.io/announcements/2021/06/02/linux-foundation-kubernetes-certifications-now-include-exam-simulator/)

   - [FAQ - CKA / CKAD & CKS](https://docs.linuxfoundation.org/tc-docs/certification/faq-cka-ckad-cks)

 ### O que é permitido?

  De acordo com a documentação, durante o exame, é permitido ao candidato:
  - revisar as intruções para o Exame, que são apresentadas no terminal
  - ler os documentos instalados pela distribuições (ex. /usr/share e seus subdiretórios)
  - use o Browser Chrome ou Chromium para abrir abas adicionais, na seguinte ordem: https://kubernetes.io/docs/, https://github.com/kubernetes/,  https://kubernetes.io/blog/ e seus subdominios. Incluindo todas as linguages disponíveis (ex. https://kubernetes.io/zh/docs/)

## Requisitos

  Conhecimentos necessários,

 - Linux / Uma boa gama de comandos para a CLI:
   - [Introdução ao Linux - Cuso gratuito - ling en](https://training.linuxfoundation.org/training/introduction-to-linux/)
   - curl / wget
   - apt / tar
   - modprobe
 - Bash
   - [Guia Bash para iniciantes - ling en](https://tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html)
   - [Programação Shell Linux](https://temporealeventos.com.br/programacao-shell-linux-com-o-mestre-julio-cezar-neves/)
 - Firewall / iptables:
   - [O que é IPTables e como usá-lo? - ling en](https://medium.com/skilluped/what-is-iptables-and-how-to-use-it-781818422e52)
   - [Guia do Iniciante em iptables, o firewall linux - ling en](https://www.howtogeek.com/177621/the-beginners-guide-to-iptables-the-linux-firewall/)
 - Docker / Containers
   - [Livro - Descomplicando Docker](https://livro.descomplicandodocker.com.br/chapters/chapter_00.html)
   - [Livro - Docker para desenvolvedores](https://leanpub.com/dockerparadesenvolvedores)
   - [Docker - From Zero to Hero - ling en](youtube.com/watch?v=3c-iBn73dDE)
   - [Containers vs VMs: Qual a diferença? - ling en](https://www.youtube.com/watch?v=cjXI-yxqGTI)

## O que é Kubernetes ???

De acordo com a documentação oficial:

> Kubernetes é um plataforma de código aberto, portável e extensiva para o gerenciamento de cargas de trabalho e serviços distribuídos em contêineres, que facilita tanto a configuração declarativa quanto a automação. Ele possui um ecossistema grande, e de rápido crescimento. Serviços, suporte, e ferramentas para Kubernetes estão amplamente disponíveis.
> -- [Kubernetes Docs](https://kubernetes.io/pt-br/docs/concepts/overview/what-is-kubernetes/)

Mais sobre:
   - [O que é Kubernetes | Kubernetes explained in 15 mins - ling en](https://www.youtube.com/watch?v=VnvRFRk_51k)
   - [Kubernetes // Dicionário do Programador](https://www.youtube.com/watch?v=mVL0nOM3AGo)
   - [O que é Kubernetes? ](https://www.redhat.com/pt-br/topics/containers/what-is-kubernetes)

Após levantar o Kubernetes, você obtem um cluster. Esse cluster é composto por Nós, com funções diferentes e bem definidas.

## Pod - Uma parte essencial

> **Pods**: are the smallest deployable units of computing that you can create and manage in Kubernetes.
> -- [Kubernetes Docs - Pods](https://kubernetes.io/docs/concepts/workloads/pods/)

Um pod compartilha o contexto de: namespaces, cgroups e demais aspectos sob uma área isolada.

É comum ficar confuso quando se compara Pods e Containers. De um modo simplificado, um container roda um microserviço e um Pod deve conter 1 ou mais containers.

Um Pod atua como uma máquina que provê diversos e distintos containers.

De modo geral, a razão para um Pod conter múltiplos containers é para poder compartilhar recursos, como rede, pontos de montagem (diretórios ou storages associados a eles).

O Kubernetes gerencia os Pods e se necessário pode criar novas instâncias do mesmo Pod, o Kubernetes aumenta o número de Pods escalonando a aplicação de maneira horizontal, conhecido como Replicação/Replication. E mais, Kubernetes provê a recuperação automática dos serviços e analiza a carga dos recursos (memória, cpu, etc...).

Abaixo um exemplo de um Pod, com um único container:

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  # Nome do pod
  name: hello
spec:
  template:
    # Esse é o template do Pod, o modelo para instância
    spec:
      containers:
      - name: hello
        # Container
        image: busybox
        command: ['sh', '-c', 'echo "Hello, Kubernetes!" && sleep 3600']
      restartPolicy: OnFailure
    # Fim do template
```

Abaixo um outro exemplo de um Pod, porém com múltiplos containers, observe que eles compartilham o mesmo dirétório.

   * este exemplo usa um diretório no Node(hospedeiro), conhecido por **ephemeral storage**

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: two-containers
spec:
  restartPolicy: Never
  volumes:
  - name: shared-data
    emptyDir: {}
  containers:
  - name: nginx-container
    image: nginx
    volumeMounts:
    - name: shared-data
      mountPath: /usr/share/nginx/html
  - name: debian-container
    image: debian
    volumeMounts:
    - name: shared-data
      mountPath: /pod-data
    command: ["/bin/sh"]
    args: ["-c", "echo Hello from the debian container > /pod-data/index.html"]
```

Referências:
  1. [Pods and Containers](https://www.youtube.com/watch?v=5cNrTU6o3Fw)
  2. [What's the difference between a pod, a cluster, and a container?](https://enterprisersproject.com/article/2020/9/pod-cluster-container-what-is-difference#:~:text=%E2%80%9CA%20container%20runs%20logically%20in,tight%20logical%20borders%20called%20namespaces.%E2%80%9D)

## Componentes Kubernetes

  > Kubernetes tem uma tonelada de componentes,  mas na maior parte do tempo você só usará alguns deles
  > -- Janashia, Nana (TechWorld)

É verdade, porém não se anime, não será tão fácil assim, afinal a **certificação cobre** todos eles, por isso será necessário aprender e entender cada um deles.

Os Nodes, citados anteriormente, tem diferentes categorias:
  - Worker - roda os Pods (um conjunto de container ativos)
    - Todo cluster deve ter **ao menos 1 Worker Nodes**
  - Control Plane - gerencia os Worker Nodes e os Pods
    - A gestão dos Pods contempla inclusive em que Worker Node ele, o Pod, deve rodar.
