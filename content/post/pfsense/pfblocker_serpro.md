+++
Categories = [
	"Sysadmin",
	"Serpro",
	"Reputation",
    "pfSense",
    "Auto",
    "pfBlocker",
    "Seguranca"
]
description = "pfSense - Usando a lista de reputação alimentada pelo Serpro"
tags = ["serpro", "firewall", "pfsense", "segurança", "block", "pfblocker"]
date = "2020-11-08"
publishdate = "2020-11-08"
menu = "pfsense"
title = "pfSense - pfBlocker e a Lista de Reputação do Serpro"
featured = "pfsense.svg"
featuredalt = ""
featuredpath = "date"
type = "post"
+++

## pfSense e pfBlocker

O pfSense é um sistema completo de firewall opensource sob a licença BSD, baseado no FreeBSD. Tem sido amplamente utilizado por diversas instituições publicas ou privadas não só pelo fato de ter seu segmento voltado para a comunidade mas também por ser de fácil uso, estável, dispor de ótimas ferramentas complementares como OpenBGPD, SquidGuard, Suricata, dentre muitos outros, além de permitir que sejam desenvolvidos pacotes que complementem suas funcionalidades, como é o caso do [pfBlocker](https://docs.netgate.com/pfsense/en/latest/packages/pfblocker.html), ferramenta que permite que se adicione ao pfSense funcionalidades como:
 - Bloqueios por IP
 - Bloqueios por Países
 - Atualização de listas baseados em agendamento
 - Flexibilidade na definição dos bloqueios ou redes e como devem ser aplicados

## Lista de Reputação do Serpro

A cada período de ocorrências ou situações, os holofotes voltam a apontar a questão da segurança das redes e como elas estão sendo aplicadas. Recentemente (Novembro de 2020), o STJ sofreu tanto a invasão quanto o sequestro de seus dados (saiba mais em [1](https://twitter.com/STJnoticias/status/1324710037324025857) | [2](https://www.uol.com.br/tilt/noticias/redacao/2020/11/07/ransomexx-virus-que-atingiu-stj-tambem-atacou-tj-pe-e-outros-paises.htm) | [3](https://olhardigital.com.br/fique_seguro/noticia/invasao-ao-stj-sequestrou-processos-e-backups-em-um-dos-piores-ciberataques-ja-vistos/109840) ), o que criou uma correta movimentação para análise das estruturas existentes.

O [Serpro](https://www.serpro.gov.br), por conta da sua importância nessa estrutura e por ser referência para orgãos federais como maior empresa pública de tecnologia de informação, tem emitido relatórios de acompanhamento e dicas de boas práticas que podem ser aplicadas nas instituições e nada impede que sejam aplicadas em empresas privadas.

A instituição mantém uma lista de reputação indicando os IPs detectados como potenciais invasores. Vamos utilizar este documento em nosso pfSense neste exemplo. Esta lista é atualizada de tempos em tempos, mas o pfSense não possui nenhuma funcionalidade nativa para atualizá-la automaticamente. Se você pensa - assim como eu - que seria interessante deixar o recurso o menos dependente de seres humanos, teremos de lançar mão de outras ferramentas agregadas nos firewall. Uma delas é o pfBlocker.

## pfBlocker e Serpro

A ideia desse post não é se aprofundar tanto nas diversas funcionalidades do pfBlocker. Vou listar só alguns pontos pertinentes à aplicação da lista de reputação. Porém, havendo interesse em obter mais detalhes, acesse [pfBlocker: Restringindo Listas IPv4, DNSBL e GeoIP](https://stato.blog.br/wordpress/pfblocker-restringindo-listas-ipv4-dnsbl-e-geoip/).

O pfBlocker aplica bloqueios baseados em: países, listas de reputação e outras regras definidas. Mas como componente adicional, antes de tudo é necessário adicioná-lo ao firewall. Vá em **System | Package Mananger | Avaliable Package** e na linha do **pfBlockerNG** clique em **"+Install"**. Imagem da instação abaixo:

{{< figure src="/img/2020/11/pfblocker_install.png" title="Instalando o pfBlocker" >}}

Após a instalação - se efetuada com sucesso - estará disponível no menu "Firewall" a opção "pfBlockerNG". entre na tela do pfBlockerNG e confirme que em "General Settings" esteja selecionada as opção "Enable pfBlockerNG".

{{< figure src="/img/2020/11/pfblocker_enable.png" title="Ativando o pfBlocker" >}}

Na mesma aba "General", na seção Interface/Rules Configuration, defina em quais interfaces de entrada e saída deverão ser aplicadas as proteções. Gosto de usar também na parte do Firewall as "Floating Rules" porque podem ser aplicadas regras comuns a diversas interfaces ao mesmo tempo, que podemos gerir a partir uma única tela. No caso, defini como entrada a WAN e saída a LAN, além do uso dinâmico nas VPNs, mas você pode aplicar a outras interfaces caso identifique a necessidade. Seguem imagens da configuração.

{{< figure src="/img/2020/11/pfblocker_inbound_outbound.png" title="Inbound e Outbound" >}}

{{< figure src="/img/2020/11/pfblocker_interface_vpn.png" title="Regras VPN e Floating Rules" >}}

Clique em **"Save"** para confirmar as mudanças.

Agora vamos definir a URL da lista de reputação. Clique na aba **"IPv4"**, o botão **"Add"** para criar o __Alias__ (ex.:BanListV4) que conterá os endereços informados pelo SERPRO, bem como a URL provida por eles **https://reputation.serpro.gov.br**. Podemos adicionar mais de uma lista, porém aqui vamos nos limitar a explicar o uso da lista do SERPRO.

{{< figure src="/img/2020/11/pfblocker_ipv4_rules.png" title="Definindo a lista IPv4 no PfBlocker" >}}

Na mesma área, definimos os parâmetros **"List Action" = Deny Both** para que sejam bloqueados os acessos em ambas as direções (inbound/outbound) e o **"Update Frequency" = Every Hour**, para que seja atualizado a cada hora. Faremos desta forma nesse primeiro momento para garantir a sincronização da informação, depois podemos aumentar o período para 2 (duas) vezes ao dia, já que o documento oficial do Serpro informa atualizações a cada 12 horas.

{{< figure src="/img/2020/11/pfblocker_ipv4_deny_update_rules.png" title="Definindo rotina de atualização e modo de aplicação dos endereços obtidos" >}}

Clique em **Save** para confirmar as alterações.

Agora o último passo, por agora: forçar a atualização da lista. Na guia **Update**, marque a opção "Reload" e clique em "Run".

{{< figure src="/img/2020/11/pfblocker_force_update.png" title="Forçar a atualização dos dados" >}}

Resultado obtido

{{< figure src="/img/2020/11/pfblocker_ipv4_deny_update_rules.png" title="Resultado da atualização" >}}

Para confirmar a aplicação, verifique a existência da lista em "Firewall >> Rules >> Floating"

{{< figure src="/img/2020/11/pfblocker_in_out_Alias.png" title="Floating" >}}

Depois, verifique no próprio pfBlocker (Firewall >> pfBlockerNG), na Guia **Alerts**, se há o registro de alguma ocorrência.

{{< figure src="/img/2020/11/pfblocker_alert.png" title="Alertas" >}}

## Conclusão

Para mim, considerando a que se destina, o pfBlockerNG apresentou eficiência e facilidade na aplicação. Espero ter ajudado com estes exemplos.
