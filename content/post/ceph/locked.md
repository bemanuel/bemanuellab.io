+++
Categories = [
	"Sysadmin", 
	"CEPH",
	"Storage",
    "Cluster"
]
description = "CEPH - Disco Travado (locked)"
tags = ["ceph", "storage","hiperconvergência","ha", "cluster", "lb", "loadbalance","raid", "image", "lock", "rbd"]
date = "2021-02-14"
publishdate = "2021-02-14"
menu = "ceph"
title = "CEPH - RBD com disco Travado"
featured = "ceph.svg"
featuredalt = ""
featuredpath = "date"
type = "post"
+++

## Descrição do problema 
   
   Temos na FIRMA um ambiente com 10 servidores Proxmox VE (PVE) fazendo a virtualização e utilizando o CEPH como Storage. Após uma pane elétrica foi necessário fazer intervenção manual pois o sistema parou por completo e o CEPH precisava refazer a sincronia e o PVE restabelecer o cluster de virtualização.

   Como um dos nós não estava entrando no ar tentei forçar a migração da VM - vou chamar aqui de vm100 - porém por alguma razão a migração não foi efetuada com sucesso, resolvi observar outras questões e depois voltar para analisar a falha de migração da vm100. Após uns 15 minutos o processo não foi concluído, permanecendo como se ainda estivesse executando, por conta da minha ansiedade resolvi reiniciar logo o node problemático.

   
>Fazendo uma rápida contextualização, o [PVE](https://proxmox.com/en/proxmox-ve) é um sistema completo de Hipervitualização que permite usar o padrão de Hiperconvergencia integrando virtualização (kvm, lxc), com storage via software (glusterfs, ceph, zfs, lvm) bem como os players de storage comuns no mercado como isci, nfs e outros, rede definida por software com ferramentas como openvswitch e outras features.

   Após essa parada a VM não subia mais e nem conseguia fazer uso dos discos/imagens dispostas no RBD ( mais sobre o [CEPH](https://blog.bemanuel.com.br/post/ceph/inicio/) ), então narro abaixo os procedimentos aplicados.

## Passos para 'destravar' os discos

   De início decidir verificar como estavam as imagens dos discos.

   * O cluster CEPH estava no ar e completamente funcional


 ```bash
 rbd info vm100-d0
 rbd info vm100-d1
 rbd info vm100-d2
 ```

  Tanto o primeiro(vm100-d0) quanto o segundo(vm100-disk-1) disco apresentaram problemas, informando alertas em invalid-object-map-related. Por isso decidi aplicar os comandos de rebuild na imagem.

  ```bash
  rbd object-map rebuild vm100-d0;rbd object-map rebuild vm100-d1
  ```
  
  Infelizmente não surtiu efeito pois o processo não iniciava. Resolvi verificar em cada um dos nós se tinha algum map para imagem ainda ativo. Falando a grosso modo o processo de map é a montagem/link entre um device no linux e a imagem da vm, permitindo assim que ó qemu acesse diretamente os discos.

  ```bash
  rbd showmapped
  ```

  Ele retorna algo nesse formato:

  id | pool | namespace | image |  snap | device
----- | :--------: | :-------: | :------: | :------: | -------:
0|rbd|-|vm220-d-0|-| /dev/rbd0
1|rbd|-|vm220-d-1|-| /dev/rbd1
10|rbd|-|vm324-d1|-| /dev/rbd10
11|rbd|-|vm324-d0|-| /dev/rbd11
12|rbd|-|vm324-d3|-|  /dev/rbd12
13|rbd|-|vm578-d0|-|  /dev/rbd13
2|rbd|-|vm670-d1|-| /dev/rbd2
3|rbd|-|vm19-d0|-| /dev/rbd3
...|...|...|...|...|...

 Nesse nó eu não identifiquei a imagem vm100-d1 ou vm100-d0 sendo usada, porém ainda tinha de testar mais 9 nós.

 Como eu pretendia verificar em todos os nós, usei o seguinte comando diretamente via ssh e filtrando pela imagem que procurava - mudando o ip a cada nó - mas de modo a acelerar a tarefa:
  ```bash
  ssh 192.168.100.x rbd showmapped  |grep vm100
  ```
 
 Infelizmente ou felizmente nenhum dos nós estava utilizando a imagem, passei então pra outro procedimento que seria verificar onde a imagem poderia estar travada/locked, __o rbd primeiro faz o lock para depois fazer o map__.

 ```bash
 rbd lock ls vm100-d0;rbd lock ls vm100-d1
 ```
 E ele retornou, para cada um dos comandos:

 ```bash
   There is 1 exclusive lock on this image.
   Locker           ID                        Address
   client.2387462   auto 98345739485748734981 10.1.100.3:0/49587394

   There is 1 exclusive lock on this image.
   Locker           ID                        Address
   client.2387489   auto 98345739485748734983 10.1.100.1:0/49587394
 ```

 Os ips diferentes se referem tanto ao nó de origem quanto ao nó de destino. Mostrando que o processo de migração realmente não foi concluído.

 Resolvi então - lembre que a VM está parada - remover o lock das imagens

 ```bash
 #rbd lock rm <imagem> <id> <locker>
 rbd lock rm vm100-d0 auto\ 98345739485748734981 client.2387462 
 rbd lock rm vm100-d0 auto\ 98345739485748734983 client.2387489 
 ```

 Repeti o comando 'lock ls' para confirmar que nenhuma estava mais travada/locked.
 E decidi refazer o 'object-map rebuild' para reconstruir a imagem.

 ```bash
 rbd object-map rebuild vm100-d0;rbd object-map rebuild vm100-d1
 ```

 Dessa vez executou com sucesso, mostrando o percentual de progresso, antes ficava parado e não dava andamento.

 Pedi a info de cada um dos discos para confirmar o rebuild e a remoção da mensagem de alerta na imagem.

```bash
rbd info vm100-d0

#Mensagem de retorno
#rbd image 'vm-110-disk-1':
#	size 850 GiB in 215200 objects
#	order 22 (4 MiB objects)
#	snapshot_count: 0
#	id: 5f57411b304a12
#	block_name_prefix: rbd_data.5f57411b304a12
#	format: 2
#	features: layering, exclusive-lock, object-map, fast-diff, deep-flatten
#	op_features:
#	flags:
#	create_timestamp: Tue Apr 17 8:38:23 2018
#	access_timestamp: Tue Apr 17 8:38:23 2018
#	modify_timestamp: Tue Apr 17 8:38:23 2018
```

Confirmado todos os passos e discos, solicitei o reinicio da vm e tudo ocorreu com sucesso, serviços reetabelecidos.

```
qm start 100
```

* Espero que esse artigo ajude alguém que possa a vir a ter o mesmo problema
* CEPH v14
* PVE 6.3
