+++
Categories = [
	"Sysadmin", 
	"NxFilter",
	"DNS",
	"Graylog",
        "Syslog",
        "UDP"
]
description = "Integrating NxFilter and Graylog using Syslog"
tags = ["nxfilter", "dns","filtro","installing","docker", "graylog","syslog" ]
date = "2016-11-11"
publishdate = "2016-11-12"
title = "NxFilter + Graylog = Reports"
featuredalt = ""
featuredpath = "date"
type = "post"
+++

# Relatórios no NxFilter

 Then, NxFilter has graphical reports and presentations, but some needs wait another types of reports.
 From this point the NxFilter service provides the implementation of Syslog to permit exports registries just in time.

## What is Syslog ?
  
 Syslog is a protocol created by IETF that permit systems to send messages using IP to some servers, the operations of users/systems. This can be used on UDP or TCP.

 Example: if you has a server on the internet that provides HTTP Service ( like Apache/NGinx ) but you want to collect the logs from this services, by this way you can write the information of access on another server.
  
 For this you need to install a syslogd server that supports Syslog and receive the data sended by the Services. Using this the logs will be saved on this new server of Logs.

## How Syslog works on NxFilter

  NxFilter, like documentation says [Syslog exportation](http://www.nxfilter.org/tutorial.html#syslog_export), formed from data separated by '|' and can be parsed into these values - following this order:

 1. Prefix - Identification 
 2. Date - Time of operation 
 3. Block - If was blocked or not
 4. Domain - Required domain - by the way, NxFilter is a DNS Server, so only the domain will appear.
 5. User - User that required 
 6. Client IP - IP of client 
 7. Policy - The applied policy ( ex. admin )
 8. Category - The applied category ( ex. news )
 9. Reason - If was blocked, the reason for the block.
 10. Query type - Code of DNS.


 Ex.: Syslog sending data to the server SyslogD, the message will be:

       NXFILTER|2013-01-28 10:53:23|Y|www.bbc.co.uk|pwuser|192.168.0.101|admin|news|Blocked by admin|33

 Que significará:
 
 * Prefix : NXFILTER
 * Date : ‘2013-01-28 10:53:23’
 * Block (Sim=y/Não=n) : Y
 * Domain : www.bbc.co.uk
 * User : pwuser
 * Client IP : 192.168.0.101
 * Policy : admin
 * Category : news
 * Reason : ‘Blocked by admin’
 * Query type : 33

## Graylog

  O serviço Graylog funciona como Centralizador de Logs. Permitindo receber logs de diversas maneiras inclusive através do modo Syslog.

  São criados Inputs e esses recebem as mensagens, permitindo gerar Extratores ( para quebrar e manipular as mensagens ) e Streams para gerar Dashboards e alertas.

  Não vou desperdiçar o tempo explicando os motivos de se utilizar um centralizador pois já foi comentado em [Graylog - Centralizador de Logs](http://blog.bemanuel.com.br/post/graylog/inicio/) e a instalação da versão mais nova do Graylog é explicada em outro post neste mesmo blog com o título [GRAYLOG 2.0 GA - CENTRALIZADOR DE LOGS](http://blog.bemanuel.com.br/post/graylog/graylog_v2/).

  Para esse post eu utilizei a imagem do Graylog para Docker [Graylog/AllInOne](https://hub.docker.com/r/graylog2/allinone/), afim de tornar mais rápida e fácil a criação deste post.

### Graylog - Input e Extractor 

  O Graylog permite que sejam criados vários canais de Input ( Entrada ) de dados, isso facilita para separarmos ou organizarmos de onde vem as informações e como estas deverão ser trabalhadas. Não digo que o modo como farei seja o mais perfeito mas é o modo como tem me atendido e essa é a beleza da ferramenta, a flexibilidade.

#### INPUT ( Graylog )
  Então o INPUT é composto basicamente do tipo a ser trabalhado, que pode ser GELF ( formato proprietário ), JSON ( para entradas de APIs HTTP ), Syslog ( que já comentamos anteriormente ) e outros. Somado ao tipo de Input, ao se definir a criação do mesmo surgirá uma nova janela pedindo informações como, isso para o caso de criar um Input do tipo ''Syslog'':

1.  O nó que escutará/receberá essas informações ( pois o Graylog pode ser formado por diversos nós )
2.  O título que será dado a esse INPUT 
3.  O IP ao qual ele será vinculado
4.  A porta que escutará o serviço 

  Há outras opções para o mesmo INPUT mas só usaremos estas para esse post.

#### EXTRACTOR ( Graylog )
  O Extractor do Graylog permite que você trabalhe as mensagens recebidas de modo a classificar e especificar a que se referem os valores.
  
  Para fazer isso o Extrator oferece diversos meios seja por usando Expressão Regular, JSON, ''Split & Index'' ( Cortar/Indexar ), Copiando a mensagem ou usando o GROK.

  Quem já trabalhou com ElasticSearch ou Logstash já conhece ou deve ter ouvido falar dele. O Grok permite que se faça uma combinação de padrões afim de extrair informações e indexa-las. A sintaxe dele é bem simples %{SINTAXE:SEMÂNTICA}. Por exemplo, se você tem a mensagem

       srvl00nxf500.bemanuel.com.br NXFILTER|2016-11-01 08:45:37

 Com o GROK você usaria os seguintes padrões pra capturar e indexar a parte da mensagem:

       %{HOSTNAME:srv} %{WORD:sys}\|%{TIMESTAMP_ISO8601:date}\|

 Você obteria:

srv | sys     | date
----- | :-----: | --------:
srvl00nxf500.bemanuel.com.br | NXFILTER | 2016-11-01 08:45:37

 Foram criados os campos/fields: srv, sys e date. Me permitindo inclusive filtrar as ocorrências por eles.

 Por conta dessa facilidade usarei o Grok. No Graylog ele já vem com diversos padrões especificados, além claro dos usados aqui no exemplo ( HOSTNAME, WORD e TIMESTAMP_ISO8601 )

 Os padrões Grok, são uma prédefinição de expressõesque auxiliam no tratamento da mensagem.

IMPORTANTE: Para ativar o pacote de padrões já disponibilizado no Graylog é preciso que acesse ''System > Content Packs'' e ative o pacote ''Core Grok Patterns''.

## Graylog e NxFilter - configurando 
 
  Vamos usar os seguintes parâmetros:

Alias  |   Descrição           |   Valor
-------|-----------------------| -------------:
SRVNXF |    Servidor NxFilter  | 192.168.1.1
SRVGRA |    Servidor Graylog   | 192.168.1.2
PRTSYS |    Porta Syslog       | 5140
----   |    Placa de rede      | ens18
----   |    Procolo do Syslog  | UDP


Lembrando que estou considerando que já instalou o Graylog.

No NxFilter não há a opção de se mudar a porta 514 e no Graylog ( a não ser que você esteja fazendo errado ) não se consegue levantar portas inferiores a 1024 sem que o serviço seja levantado pelo usuário ''root''.

`Se já usa a versão 3.4.6 ou superior os passos abaixo já não são mais necessários pois o NxFilter a partir da 3.4.6 permite definir a porta a ser usada`

Para que isso funcione faremos o redirecionamento de solicitações da porta 514 para a porta 5140 - que será a utilizada neste post, conforme especificado acima.

    sudo iptables -A PREROUTING -t nat -i ens18 -p udp --dport 514 -j REDIRECT --to-port 5140 

`Esse comando deve ser executado no servidor Graylog com o usuário root`

No servidor NxFilter tudo que é necessário ser feito é ir em `Config > Setup` e definir o ip do servidor Graylog.

{{< figure src="/img/2016/11/graylog_nxfilter_syslog.png" title="Configurando o serviço Syslog no NxFilter" >}}

Após esse procedimento é necessário reiniciar o NxFilter.

A partir daqui todo o processo é feito no Graylog.

No Graylog temos de fazer os seguintes passos:

1. Criar o INPUT
2. Aplicar um Extractor na mensagem recebida
3. Criar os Gráficos

## Criando o INPUT

O INPUT é a parte base do sistema de gerenciamento de logs, através dele que são coletadas as informações. Será criado um INPUT para a entrada de registros enviados pelo NxFilter e conforme definido anteriormente a porta usada será a 5140.

Acesse o menu em ''System > Inputs'', lá você terá acesso a todos os Inputs registrados.

{{< figure src="/img/2016/11/graylog_nxfilter_system_inputs.png" title="System >  Inputs" >}}

Retornando a listagem de inputs existentes e suas estatísticas. Para criar o seu input acesse o combo com a listagem de tipos de input, escolho o modelo ''Syslog UDP'' e clique em ''Launch new input''. 

{{< figure path="date" file="graylog_nxfilter_criando_input_00.png" alt="Combo Input" type="center" >}}

{{< figure path="date" file="graylog_nxfilter_criando_input.png" alt="Criando Input" type="center" >}}

Em seguida aparecerá uma janela solicitando a definição de parâmetros para esse INPUT. Defina o título/nome do input como ''NxFilter'' e em `Node` você deve escolher que servidor do Graylog receberá os registros do syslog para isso basta clicar na seta do combo e aparecerá a listagem dos `nodes` registrados, deixe `Bind address` com o valor padrão '0.0.0.0' e em port - que é a porta que o serviço deverá abrir para reveber os registros - sete o valor 5140.

Há ainda outros parâmetros mas para o momento somente esses interessam.

Deverá aparecer um item como o abaixo...


{{< figure src="/img/2016/11/graylog_nxfilter_criando_input_01.png" title="Definindo parâmetros do INPUT" >}}

Após a criação do Input deverá aparecer o item contendo:
1. título e protocolo definidos
2. a porta
3. Botões para: Exibir as mensagens, Gerenciar Extratores, Parar o Input e outros...

{{< figure src="/img/2016/11/graylog_nxfilter_input_criado.png" title="Resultado da criação do Input" >}}

Neste ponto se estiver tudo correto o item Input recém criado deverá exibir mensagens como a abaixo ao se clicar em ''Show received messages''.

{{< figure src="/img/2016/11/graylog_nxfilter_msgs.png" title="Mensagens capturadas" >}}

Agora é o momento de tratá-las usando o ''Extractor'' que nos auxiliará trabalhando a mensagem. 

## Extractor - creating

Its functions is manipulate the text/message received to split it on 'fields' defined by the user.

Uma das formas de criá-lo é na área do Input, clicando no botão 'Show messages', como a imagem mostrada acima. Essa operação fará com que se abra uma tela contendo diversas mensagens recebidas pelo input 'NxFilter'.

Clique na mensagem e aparecerá o campo ''message'' no canto direito há uma espécie de lupa. Ao clicar nela aparecerão diversas opções, dentre elas ''Create extractor field message'', selecionado essa opção aparecerá um submenu.

No submenu escolha ''Grok pattern', que será o padrão que usaremos para extrair as informações e atribuir aos campos desejados.

IMPORTANTE: Para ativar o pacote de padrões já disponibilizado no Graylog é preciso que acesse ''System > Content Packs'' e ative o pacote ''Core Grok Patterns''.

Deixe marcado o campo 'Named captures only', desse modo só os resultados recebidos na análise do padrão serão exibidos.

Em 'Grok Pattern' insira - exatamente - a seguinte expressão, tudo em uma linha só:
```
%{HOSTNAME:srv} %{WORD:sys}\|%{TIMESTAMP_ISO8601:date}\|%{WORD:block}\|%{HOSTNAME:domain}\
|%{GREEDYDATA:user}\|%{IP:src_ip}\|%{GREEDYDATA:policy}\|%{GREEDYDATA:category}\|%{GREEDYDATA:reason}\|%{INT:dns_type}
```

Clique no botão 'Try', se estiver tudo certo aparecerá um item ''Extractor preview'' mostrando a previsão do resultado da análise da mensagem, parecido como abaixo:

{{< figure path="date" file="graylog_nxfilter_extractor_pattern.png" type="center" >}}

No campo 'Condition' é definido quando e o Extractor atuará, marque a 3a opção:

- [ ] 'Always try to extract' - Sempre executar o Extractor
- [ ] 'Only attempt extraction if field contains string' - Somente se encontrar uma determinada ocorrência
- [x] 'Only attempt extraction if field matches regular expression' - Somente se casar com uma determinada expressão regular

E preencha o campo 'Field matches regular expression', com o valor:
   ^*NXFILTER\|*

Se estiver tudo correto, ao clicar no botão 'Try', que fica a direita do 'Field matches...', deverá receber um resultado como o da imagem abaixo.

{{< figure src="/img/2016/11/graylog_nxfilter_extractor_01.png" >}}

Em 'Extraction strategy' deixe marcado 'Copy', pois segundo o Graylog o procedimento de recorte ('Cut') não funciona em campos como o 'message'.

Em 'Extractor title' preencha com ''NxFilter''. Clique em 'Create extractor'. Irá para uma tela onde listam os Extractors para esse input, no caso deverá aparecer somente o 'NxFilter'.

Clique em 'Details', se estiver tudo certo aparecerá como a tela abaixo.

{{< figure src="/img/2016/11/graylog_nxfilter_extractor_02.png" >}}

Estando o serviço do NxFilter configurado para enviar as mensagens para o Graylog, já poderão ser vistas mensagens do NxFilter. A princípio parecerão as mesmas mensagens antes apresentadas, mas na área 'Fields' estarão listados os campos que criamos com o 'Extractor' como 'block' e 'reason'.

{{< figure src="/img/2016/11/graylog_nxfilter_search_result.png" >}}


## Creating a Dashboard for NxFilter Service

 Now you can create a Dashboard with all the informations of DNS Usage on your network.

 No canto esquerdo dos campos há uma seta, clicando nela aparece um quadro com opções.

 . Generate Chart - Gera um gráfico, só funciona para campos númericos
 . Quick values - Gera um gráfico pizza, contendo estatísticas simples como quantidade/percentual de um determinado registro
 . Statistics - Estatísticas mostrando quantidades máximas, mínimas, desvio padrão, etc...
 . World Map - Se o campo for IP e estiver ativado o recurso de GeoIP ele apresenta um mapa indicando a origem dos mesmos, desde que sejam IPs válidos. 


{{< figure src="/img/2016/11/graylog_nxfilter_search_graphs_00.png" >}}

Tendo gerado a informação que julgue interessante você pode jogar ele em um Dashboard existente ou criar um novo caso não exista nenhum.

On new graphic click in 'Add to Dashboard', you can choice an existent Dashboard.

 If want to create a new Dashboard, go on menu 'Dashboards' and click on 'Create Dashboard'.

{{< figure src="/img/2016/11/graylog_nxfilter_search_graphs_01.png" >}}

On Dashboard creation, the system ask about Title and Description required fields.

{{< figure src="/img/2016/11/graylog_nxfilter_criando_dashboard_00.png" >}}

After this is displayed a panel asking Title for the Widget. The choices:

[] Show pie chart - Exibe gráfico torta
[] Show data table - Exibe dados

Choice what do you want - this can be changed later - click on ''Create''.

{{< figure src="/img/2016/11/graylog_nxfilter_criando_dashboard.png" >}}

This gonna show the Dashboard with Widget. Some examples:

{{< figure src="/img/2016/11/graylog_nxfilter_dashboard_completo.png" >}}

{{< figure src="/img/2016/11/graylog_nxfilter_dashboard_gerado.png" >}}

 Now work these data do show on the better way.
