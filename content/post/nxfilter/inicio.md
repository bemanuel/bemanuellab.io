+++
Categories = [
	"Sysadmin",
	"NxFilter",
	"DNS"
]
description = "Sobre o que é a ferramenta e como instalar"
tags = ["nxfilter", "dns","filtro","instalando","raspbian", "debian", "pfsense" ]
date = "2016-04-12"
publishdate = "2016-04-15"
title = "NXFilter - O Filtro DNS"
featured = "nxfilter01.png"
featuredalt = ""
featuredpath = "date"
type = "post"
+++

#### Apresentação

O [NXFilter](https://nxf.kernel.inf.br) é uma ferramenta que começou com a ideia de atuar como filtro DNS e agora provê também a possibilidade de filtro de conteúdo web.

A documentação completa da ferramenta está no [tutorial](http://docs.nxf.kernel.inf.br) já traduzido.

Dentre as vantagens disponibilizadas se tem:

 1. É uma ferramenta leve e de fácil instalação
 2. Controle por autenticação usando: LDAP, AD, Single-sign-on ( SSO ), etc...
 3. Pode substituir inclusive o seu proxy-cache como o Squid
 4. Usando outros componentes permite inclusive o bloqueio de ferramentas como UltraSurf e Tor
 5. Reconhecimento dinâmico de sites, não depende apenas de listas, encontra o padrão e a classifica

#### Funcionamento

Seu principio é atuar como servidor DNS, pode fazer:

 1. Redirect
 2. Transferência de Zona
 3. Domínio Dinâmico
 4. E inclusive como o próprio servidor DNS, permitindo registros A, AAAA, SOA e etc...


#### Instalando

A instalação do NxFilter é bem simplificada, o site disponibiliza pacotes deb,zip e outros formatos depreciados como exe.

Como pré-requisito só é necessário ter o java instalado na máquina, funciona tanto com OpenJDK quanto com o Oracle Java.

 * Downloads na página oficial do [NxFilter](https://nxf.kernel.inf.br/download/)

##### Debian Jessie / Raspbian
```bash
$ sudo su -
$ apt-get update
$ apt-get install openjdk-8-jre-headless
$ wget -t0 -c http://nxfilter.org/download/nxfilter-4.1.3.deb
$ dpkg -i nxfilter-4.1.3.deb
```

##### Arquivo ZIP
```bash
$ sudo su -
$ apt-get update
$ apt-get install openjdk-8-jre-headless
$ mkdir /nxfilter && cd /nxfilter
$ wget -t0 -c http://nxfilter.org/download/nxfilter-4.1.3.zip
$ unzip nxfilter-4.1.3.zip
$ cd /nxfilter/bin
$ chmod +x *.sh
$ ./startup.sh
```


##### PFSense 2.2.6 - Dica do Grupo NxFilter - DNS WebFilter no Telegram (verifique atualização em [post sobre o NxFilter no pfSense](http://blog.bemanuel.com.br/post/nxfilter/nxfilter_pfsense_2_3/))
```bash
$ pkg update
$ pkg install openjdk8-jre
$ rehash
$ mkdir -p /opt/nxfilter
$ cd /opt/nxfilter
$ fetch http://nxfilter.org/download/nxfilter-4.1.3.zip
$ unzip nxfilter-4.1.3.zip
$ cd bin
$ chmod +x *.sh
$ ./startup.sh -d
```

##### Terminada a instalação

 Ao término da instalação a ferramenta já pode ser utilizada como servidor DNS na rede. Para administrar acesse ``http://ip_do_servidor_nxfilter/admin``
