+++
Categories = [
	"Sysadmin",
	"NxFilter",
	"DNS",
	"OpenDNS",
	"AD",
	"Empresa",
	"Enterprise",
	"Filter",
	"Filtro",
]
description = "Instalando o NxFilter na sua empresa, integrado com o AD"
tags = ["nxfilter", "dns","filtro","instalando","raspbian", "debian", "pfsense" ]
date = "2017-07-01"
publishdate = "2017-07-03"
title = "NXFilter autenticando em AD"
featured = "nxfilter01.png"
featuredalt = ""
featuredpath = "date"
type = "post"
+++

#### Apresentação

O [NXFilter](https://nxf.kernel.inf.br) é uma ferramenta que começou com a ideia de atuar como filtro DNS e agora provê também a possibilidade de filtro de conteúdo web.

A documentação completa da ferramenta está no [tutorial](http://docs.nxf.kernel.inf.br) já traduzido.

Dentre as vantagens disponibilizadas se tem:

 1. É uma ferramenta leve e de fácil instalação
 2. Controle por autenticação usando: LDAP, AD, Single-sign-on ( SSO ), etc...
 3. Pode substituir inclusive o seu proxy-cache como o Squid, em determinadas funções
 4. Usando outros componentes permite inclusive o bloqueio de ferramentas como UltraSurf e Tor
 5. Reconhecimento dinâmico de sites, não depende apenas de listas, encontra o padrão e a classifica
 6. Detectar trojans na sua rede

#### Funcionamento

Seu principio é atuar como servidor DNS, pode fazer:

 1. Redirect
 2. Transferência de Zona
 3. Domínio Dinâmico
 4. E inclusive como o próprio servidor DNS, permitindo registros A, AAAA, SOA e etc...

#### Instalação

 Siga os principios descritos na [documentação oficial](http://docs.nxf.kernel.inf.br/pt_BR/latest/pages/getting_started/install.html) ou já explicado [aqui](https://blog.bemanuel.com.br/post/nxfilter/inicio/) no blog

#### Configurando
  DNS AD , vai bloquear?
